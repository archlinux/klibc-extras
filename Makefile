
PROGS = kill lodel losetup mdassemble moddeps mv parseblock replace resolve-modalias
OBJS = devname.o name_to_dev.o
HEADERS = devname.h list.h

DESTDIR =
PREFIX = /usr/lib/klibc/bin

MKDIR = /bin/mkdir
INSTALL = /bin/install -c -m 755
PWD = $(shell pwd)

KLCC = /usr/bin/klcc
CC   = $(KLCC)
LD   = $(KLCC)
STRP = /usr/bin/strip

CFLAGS   = -g -O2 -pipe -D_FILE_OFFSET_BITS=64 #-DDEBUG
WARNINGS = -Wall -Wstrict-prototypes -Wsign-compare -Wchar-subscripts \
           -Wpointer-arith -Wcast-align -Wsign-compare
CFLAGS  += $(WARNINGS)
LDFLAGS =

#pretty print!
E = @echo
Q = @

all: $(PROGS)
.PHONY: all
.DEFAULT: all

%.o: %.c
	$(E) "  compile " $@
	$(Q) $(CC) -c $(CFLAGS) $< -o $@

kill: kill.o $(HEADERS) $(OBJS)
	$(E) ">>build   " $@
	$(Q) $(LD) $(LDFLAGS) $@.o $(OBJS) -o $@ $(LIB_OBJS)
	$(Q) $(STRP) $@

lodel: lodel.o $(HEADERS) $(OBJS)
	$(E) ">>build   " $@
	$(Q) $(LD) $(LDFLAGS) $@.o $(OBJS) -o $@ $(LIB_OBJS)
	$(Q) $(STRP) $@

losetup: losetup.o $(HEADERS) $(OBJS)
	$(E) ">>build   " $@
	$(Q) $(LD) $(LDFLAGS) $@.o $(OBJS) -o $@ $(LIB_OBJS)
	$(Q) $(STRP) $@

mdassemble: mdassemble.o $(HEADERS) $(OBJS)
	$(E) ">>build   " $@
	$(Q) $(LD) $(LDFLAGS) $@.o $(OBJS) -o $@ $(LIB_OBJS)
	$(Q) $(STRP) $@

moddeps: moddeps.o $(HEADERS) $(OBJS)
	$(E) ">>build   " $@
	$(Q) $(LD) $(LDFLAGS) $@.o $(OBJS) -o $@ $(LIB_OBJS)
	$(Q) $(STRP) $@

mv: mv.o $(HEADERS) $(OBJS)
	$(E) ">>build   " $@
	$(Q) $(LD) $(LDFLAGS) $@.o $(OBJS) -o $@ $(LIB_OBJS)
	$(Q) $(STRP) $@

parseblock: parseblock.o $(HEADERS) $(OBJS)
	$(E) ">>build   " $@
	$(Q) $(LD) $(LDFLAGS) $@.o $(OBJS) -o $@ $(LIB_OBJS)
	$(Q) $(STRP) $@

replace: replace.o $(HEADERS) $(OBJS)
	$(E) ">>build   " $@
	$(Q) $(LD) $(LDFLAGS) $@.o $(OBJS) -o $@ $(LIB_OBJS)
	$(Q) $(STRP) $@

resolve-modalias: resolve-modalias.o $(HEADERS) $(OBJS)
	$(E) ">>build   " $@
	$(Q) $(LD) $(LDFLAGS) $@.o $(OBJS) -o $@ $(LIB_OBJS)
	$(Q) $(STRP) $@

clean:
	$(E) "  clean   "
	$(Q) rm -f $(PROGS) *.o
.PHONY: clean

install: all
	$(MKDIR) -p $(DESTDIR)$(PREFIX)/
	$(INSTALL) $(PROGS) $(DESTDIR)$(PREFIX)/
.PHONY: install

uninstall:
	cd $(DESTDIR)$(PREFIX)/
	rm $(PROGS)
.PHONY: uninstall
