/*
    name_to_dev_t.h : convert a block device name to major/minor
        numbers. Most of this code is taken from the kernel
        init/ and kinit source.
    Copyright (C) 2006, Aaron Griffin <aaronmgriffin@gmail.com>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/
#ifndef _DEVNAME_H_
#define _DEVNAME_H_

#include <sys/types.h>
#include <sys/sysmacros.h>
#include <sys/stat.h>

#define	Root_RAM0	__makedev(1,0)

/* These device numbers are only used internally */
#define Root_NFS	__makedev(0,255)
#define Root_MTD	__makedev(0,254)

#define BUF_SZ		65536
#define TMPBUF_SZ	512

dev_t try_name(char *, int);

dev_t name_to_dev_t(const char *);

int scansysdir(char *, char *, dev_t);

#endif /* _DEVNAME_H_ */
