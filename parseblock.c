/*
    parseblock: parse a block device node name.  Made for
        early userspace under klibc.  Most of this code is taken
        from the kernel init/ and kinit source.
    Copyright (C) 2006, Aaron Griffin <aaronmgriffin@gmail.com>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "devname.h"

int main(int argc, char *argv[])
{
	char sysdir[TMPBUF_SZ];
    dev_t blockdev = 0;
	char blockname[TMPBUF_SZ];

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <block device name>\n", argv[0]);
    }
    else {
        blockdev = name_to_dev_t(argv[1]);

        if (blockdev) {
            if (blockdev == Root_NFS) {
                strcpy(blockname, "nfs");
            }
            else {
                strcpy(sysdir, "/sys/block");

                if (!scansysdir(blockname, sysdir, blockdev))
                    strcpy(blockname, "root");
            }

            printf("BLOCKNAME=\"/dev/%s\"\nBLOCKDEVICE=\"%d %d\"\n",
                   blockname,
                   major(blockdev),minor(blockdev));
            return 0;
        }
    }
    printf("BLOCKNAME=\"unknown\"\nBLOCKDEVICE=\"\"\n");
    return 1;
}
